import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { JwtToken } from "../model/jwt-token";
import { Message } from "../model/message";
import { User } from "../model/user";

const AUTH_PATH = `${environment.rest_path}/auth/sign`;

const httpHeaders = {
    headers: new HttpHeaders({"Content-Type": "application/json"})
};

@Injectable({
    providedIn: "root"
})
export class AuthService {
    public constructor(private readonly http: HttpClient) {
    }

    public logInUser(user: User): Observable<JwtToken> {
        return this.http.post<JwtToken>(`${AUTH_PATH}/in`, user, httpHeaders);
    }

    public signUpUser(user: User): Observable<Message> {
        return this.http.post<Message>(`${AUTH_PATH}/up`, user, httpHeaders);
    }
}
