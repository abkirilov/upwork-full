export const multi = [
    {
        name: "Germany",
        series: [
            {
                name: "1990",
                value: 62000000
            },
            {
                name: "2010",
                value: 73000000
            },
            {
                name: "2011",
                value: 89400000
            }
        ]
    },
    {
        name: "USA",
        series: [
            {
                name: "1990",
                value: 250000000
            },
            {
                name: "2010",
                value: 309000000
            },
            {
                name: "2011",
                value: 311000000
            }
        ]
    },
    {
        name: "France",
        series: [
            {
                name: "1990",
                value: 58000000
            },
            {
                name: "2010",
                value: 50000020
            },
            {
                name: "2011",
                value: 58000000
            }
        ]
    },
    {
        name: "UK",
        series: [
            {
                name: "1990",
                value: 57000000
            },
            {
                name: "2010",
                value: 62000000
            }
        ]
    }
];

export const single = [
    {
        name: "Germany",
        value: 8940000
    },
    {
        name: "USA",
        value: 5000000
    },
    {
        name: "France",
        value: 7200000
    }
];

export const sensorLine = [
    {
        name: "Sensor1",
        series: [
            {
                name: "00:00",
                value: getRandomTemp()
            },
            {
                name: "01:00",
                value: getRandomTemp()
            },
            {
                name: "02:00",
                value: getRandomTemp()
            },
            {
                name: "03:00",
                value: getRandomTemp()
            },
            {
                name: "04:00",
                value: getRandomTemp()
            },
            {
                name: "05:00",
                value: getRandomTemp()
            },
            {
                name: "06:00",
                value: getRandomTemp()
            },
            {
                name: "07:00",
                value: getRandomTemp()
            },
            {
                name: "08:00",
                value: getRandomTemp()
            },
            {
                name: "09:00",
                value: getRandomTemp()
            },
            {
                name: "10:00",
                value: getRandomTemp()
            },
            {
                name: "11:00",
                value: getRandomTemp()
            },
            {
                name: "12:00",
                value: getRandomTemp()
            },
            {
                name: "13:00",
                value: getRandomTemp()
            },
            {
                name: "14:00",
                value: getRandomTemp()
            },
            {
                name: "15:00",
                value: getRandomTemp()
            },
            {
                name: "16:00",
                value: getRandomTemp()
            },
            {
                name: "17:00",
                value: getRandomTemp()
            },
            {
                name: "18:00",
                value: getRandomTemp()
            },
            {
                name: "19:00",
                value: getRandomTemp()
            },
            {
                name: "20:00",
                value: getRandomTemp()
            },
            {
                name: "21:00",
                value: getRandomTemp()
            },
            {
                name: "22:00",
                value: getRandomTemp()
            },
            {
                name: "23:00",
                value: getRandomTemp()
            }
        ]
    },
    {
        name: "Sensor2",
        series: [
            {
                name: "00:00",
                value: getRandomTemp()
            },
            {
                name: "01:00",
                value: getRandomTemp()
            },
            {
                name: "02:00",
                value: getRandomTemp()
            },
            {
                name: "03:00",
                value: getRandomTemp()
            },
            {
                name: "04:00",
                value: getRandomTemp()
            },
            {
                name: "05:00",
                value: getRandomTemp()
            },
            {
                name: "06:00",
                value: getRandomTemp()
            },
            {
                name: "07:00",
                value: getRandomTemp()
            },
            {
                name: "08:00",
                value: getRandomTemp()
            },
            {
                name: "09:00",
                value: getRandomTemp()
            },
            {
                name: "10:00",
                value: getRandomTemp()
            },
            {
                name: "11:00",
                value: getRandomTemp()
            },
            {
                name: "12:00",
                value: getRandomTemp()
            },
            {
                name: "13:00",
                value: getRandomTemp()
            },
            {
                name: "14:00",
                value: getRandomTemp()
            },
            {
                name: "15:00",
                value: getRandomTemp()
            },
            {
                name: "16:00",
                value: getRandomTemp()
            },
            {
                name: "17:00",
                value: getRandomTemp()
            },
            {
                name: "18:00",
                value: getRandomTemp()
            },
            {
                name: "19:00",
                value: getRandomTemp()
            },
            {
                name: "20:00",
                value: getRandomTemp()
            },
            {
                name: "21:00",
                value: getRandomTemp()
            },
            {
                name: "22:00",
                value: getRandomTemp()
            },
            {
                name: "23:00",
                value: getRandomTemp()
            }
        ]
    },
    {
        name: "Sensor3",
        series: [
            {
                name: "00:00",
                value: getRandomTemp()
            },
            {
                name: "01:00",
                value: getRandomTemp()
            },
            {
                name: "02:00",
                value: getRandomTemp()
            },
            {
                name: "03:00",
                value: getRandomTemp()
            },
            {
                name: "04:00",
                value: getRandomTemp()
            },
            {
                name: "05:00",
                value: getRandomTemp()
            },
            {
                name: "06:00",
                value: getRandomTemp()
            },
            {
                name: "07:00",
                value: getRandomTemp()
            },
            {
                name: "08:00",
                value: getRandomTemp()
            },
            {
                name: "09:00",
                value: getRandomTemp()
            },
            {
                name: "10:00",
                value: getRandomTemp()
            },
            {
                name: "11:00",
                value: getRandomTemp()
            },
            {
                name: "12:00",
                value: getRandomTemp()
            },
            {
                name: "13:00",
                value: getRandomTemp()
            },
            {
                name: "14:00",
                value: getRandomTemp()
            },
            {
                name: "15:00",
                value: getRandomTemp()
            },
            {
                name: "16:00",
                value: getRandomTemp()
            },
            {
                name: "17:00",
                value: getRandomTemp()
            },
            {
                name: "18:00",
                value: getRandomTemp()
            },
            {
                name: "19:00",
                value: getRandomTemp()
            },
            {
                name: "20:00",
                value: getRandomTemp()
            },
            {
                name: "21:00",
                value: getRandomTemp()
            },
            {
                name: "22:00",
                value: getRandomTemp()
            },
            {
                name: "23:00",
                value: getRandomTemp()
            }
        ]
    },
    {
        name: "Sensor4",
        series: [
            {
                name: "00:00",
                value: getRandomTemp()
            },
            {
                name: "01:00",
                value: getRandomTemp()
            },
            {
                name: "02:00",
                value: getRandomTemp()
            },
            {
                name: "03:00",
                value: getRandomTemp()
            },
            {
                name: "04:00",
                value: getRandomTemp()
            },
            {
                name: "05:00",
                value: getRandomTemp()
            },
            {
                name: "06:00",
                value: getRandomTemp()
            },
            {
                name: "07:00",
                value: getRandomTemp()
            },
            {
                name: "08:00",
                value: getRandomTemp()
            },
            {
                name: "09:00",
                value: getRandomTemp()
            },
            {
                name: "10:00",
                value: getRandomTemp()
            },
            {
                name: "11:00",
                value: getRandomTemp()
            },
            {
                name: "12:00",
                value: getRandomTemp()
            },
            {
                name: "13:00",
                value: getRandomTemp()
            },
            {
                name: "14:00",
                value: getRandomTemp()
            },
            {
                name: "15:00",
                value: getRandomTemp()
            },
            {
                name: "16:00",
                value: getRandomTemp()
            },
            {
                name: "17:00",
                value: getRandomTemp()
            },
            {
                name: "18:00",
                value: getRandomTemp()
            },
            {
                name: "19:00",
                value: getRandomTemp()
            },
            {
                name: "20:00",
                value: getRandomTemp()
            },
            {
                name: "21:00",
                value: getRandomTemp()
            },
            {
                name: "22:00",
                value: getRandomTemp()
            },
            {
                name: "23:00",
                value: getRandomTemp()
            }
        ]
    }
];

export const sensorBar = [
    {
        name: "Sensor1",
        value: getRandomTemp()
    },
    {
        name: "Sensor2",
        value: getRandomTemp()
    },
    {
        name: "Sensor3",
        value: getRandomTemp()
    },
    {
        name: "Sensor4",
        value: getRandomTemp()
    }
];

function getRandomTemp(): number {
    return Math.floor(Math.random() * 101) - 50;
}
