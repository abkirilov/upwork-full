import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

const AUTH_TEST_PATH = `${environment.rest_path}/auth/test`;

@Injectable({
    providedIn: "root"
})
export class AuthTestService {
    public constructor(private readonly http: HttpClient) {
    }

    public getPublicContent(): Observable<string> {
        return this.http.get(`${AUTH_TEST_PATH}/all`, {responseType: "text"});
    }

    public getUserContent(): Observable<string> {
        return this.http.get(`${AUTH_TEST_PATH}/user`, {responseType: "text"});
    }

    public getModeratorBoard(): Observable<string> {
        return this.http.get(`${AUTH_TEST_PATH}/moderator`, {responseType: "text"});
    }

    public getAdminBoard(): Observable<string> {
        return this.http.get(`${AUTH_TEST_PATH}/admin`, {responseType: "text"});
    }
}
