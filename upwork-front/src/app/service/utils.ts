export class Utils {
    public static getErrorMessage(err: any): string {
        if (err.error !== null && err.error !== undefined) {
            return err.error.message;
        }
        return "Server is not responding";
    }

    public static getErrorText(err: any): string {
        try {
            return JSON.parse(err.error).message;
        } catch (e) {
            return Utils.getErrorMessage(err);
        }
    }

    public static includes(text: any, val: string): boolean {
        return text.indexOf(val) !== -1;
    }
}
