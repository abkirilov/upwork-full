import { Injectable } from "@angular/core";
import { JwtToken } from "../model/jwt-token";

const JWT_TOKEN = "JWT_TOKEN";

@Injectable({
    providedIn: "root"
})
export class TokenStorageService {
    private readonly sessionStorage: Storage = window.sessionStorage;

    public saveJwtToken(jwtToken: JwtToken): void {
        this.sessionStorage.removeItem(JWT_TOKEN);
        this.sessionStorage.setItem(JWT_TOKEN, JSON.stringify(jwtToken));
    }

    public getJwtToken(): JwtToken {
        return JSON.parse(this.sessionStorage.getItem(JWT_TOKEN));
    }

    public getToken(): string {
        const jwtToken = this.getJwtToken();
        return jwtToken !== null ? this.getJwtToken().token : null;
    }

    public clearSessionStorage(): void {
        this.sessionStorage.clear();
        window.location.reload();
    }
}
