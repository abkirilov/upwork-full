import { Component, OnInit } from "@angular/core";
import { JwtToken } from "../../model/jwt-token";
import { TokenStorageService } from "../../service/token-storage.service";

@Component({
    templateUrl: "./profile.component.html"
})
export class ProfileComponent implements OnInit {
    private jwtToken: JwtToken = new JwtToken();
    private roles = "";

    public constructor(
        private readonly tokenStorageService: TokenStorageService
    ) {
    }

    public ngOnInit(): void {
        this.jwtToken = this.tokenStorageService.getJwtToken();
        this.roles = this.jwtToken !== null ? this.jwtToken.roles.join(", ") : "";
    }
}
