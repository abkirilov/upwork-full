import { Component, OnInit } from "@angular/core";
import { DataSharingService } from "../../service/data-sharing.service";
import { TokenStorageService } from "../../service/token-storage.service";
import { Utils } from "../../service/utils";

@Component({
    selector: "app-navbar",
    templateUrl: "./navbar.component.html",
    styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit {
    private isCollapse = true;
    private isLogIn = false;
    private roles: string[] = [];
    private showUserContent = false;
    private showModeratorBoard = false;
    private showAdminBoard = false;
    private username = "";

    public constructor(
        private readonly dataSharingService: DataSharingService,
        private readonly tokenStorageService: TokenStorageService
    ) {
        this.dataSharingService.isLogIn.subscribe((data) => {
            this.loadJwtToken();
        });
    }

    public ngOnInit(): void {
        this.loadJwtToken();
    }

    private loadJwtToken(): void {
        const jwtToken = this.tokenStorageService.getJwtToken();
        if (jwtToken !== null) {
            this.isLogIn = true;
            this.roles = jwtToken.roles;
            this.showUserContent = Utils.includes(this.roles, "ROLE_USER");
            this.showModeratorBoard = Utils.includes(this.roles, "ROLE_MODERATOR");
            this.showAdminBoard = Utils.includes(this.roles, "ROLE_ADMIN");
            this.username = jwtToken.username;
        }
    }

    private toggleCollapse(): void {
        this.isCollapse = !this.isCollapse;
    }

    private signOut(): void {
        this.tokenStorageService.clearSessionStorage();
    }
}
