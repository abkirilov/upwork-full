import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap";
import { environment } from "../../../../environments/environment";
import { ErrorNotification } from "../../../model/error-notification";

@Component({
    templateUrl: "./error-notification.component.html",
    styleUrls: ["./error-notification.component.scss"]
})
export class ErrorNotificationComponent implements OnInit {
    private readonly errorNotification: ErrorNotification = new ErrorNotification();

    public constructor(private readonly bsModalRef: BsModalRef) {
    }

    public ngOnInit(): void {
        setTimeout(() => {
            this.bsModalRef.hide();
        }, environment.timeout_notification);
    }
}
