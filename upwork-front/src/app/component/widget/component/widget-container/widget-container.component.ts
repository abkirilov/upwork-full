import { Component, ComponentFactory, ComponentFactoryResolver, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { WidgetDirective } from "../../directive/widget.directive";
import { WidgetComponent } from "../../model/widget-component";
import { WidgetItem } from "../../model/widget-item";

@Component({
    selector: "app-widget-container",
    styleUrls: ["./widget-container.component.scss"],
    templateUrl: "./widget-container.component.html",
})
export class WidgetContainerComponent implements OnInit {
    @Input() public widgetItem: WidgetItem;
    @Output() public readonly removeWidgetChange: EventEmitter<WidgetItem> = new EventEmitter();
    @ViewChild(WidgetDirective, {static: true}) public widgetDirective: WidgetDirective;

    private componentFactory: ComponentFactory<WidgetComponent>;
    private widgetComponent: WidgetComponent;

    public constructor(
        private readonly componentFactoryResolver: ComponentFactoryResolver
    ) {
    }

    public ngOnInit(): void {
        this.componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.widgetItem.component);
        this.widgetComponent = this.widgetDirective.viewContainerRef.createComponent(this.componentFactory).instance;
        this.widgetComponent.data = this.widgetItem.data;
    }

    private openSettings(): void {
        this.widgetComponent.openSettings().subscribe(
            (data) => {
                if (data !== null && data !== undefined) {
                    this.widgetComponent.data = data;
                    this.widgetComponent.reloadData();
                }
            }
        );
    }

    private removeWidget(): void {
        this.removeWidgetChange.emit(this.widgetItem);
    }
}
