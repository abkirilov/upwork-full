import { Component } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { Subject } from "rxjs";
import { NotebookData } from "../../model/notebook-data";
import { WidgetComponent } from "../../model/widget-component";
import { NotebookSettingsComponent } from "../modal/notebook-settings/notebook-settings.component";

@Component({
    styleUrls: ["./notebook-widget.component.scss"],
    templateUrl: "./notebook-widget.component.html",
})
export class NotebookWidgetComponent implements WidgetComponent {

    public data: NotebookData;
    private bsModalRef: BsModalRef;

    public constructor(private readonly bsModalService: BsModalService) {
    }

    public openSettings(): Subject<NotebookData> {
        this.bsModalRef = this.bsModalService.show(NotebookSettingsComponent, {
            initialState: {
                data: this.data
            }
        });
        return this.bsModalRef.content.onClose;
    }

    public reloadData(): void {
    }
}
