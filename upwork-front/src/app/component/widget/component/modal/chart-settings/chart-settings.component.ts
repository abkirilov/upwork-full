import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap";
import { Subject } from "rxjs";
import { ChartData } from "../../../model/chart-data";
import { ChartPeriod } from "../../../model/chart-period";
import { ChartType } from "../../../model/chart-type";
import { ColorScheme } from "../../../model/color-scheme";
import { ColorSchemeService } from "../../../service/color-scheme.service";

@Component({
    templateUrl: "./chart-settings.component.html"
})
export class ChartSettingsComponent implements OnInit {
    private readonly onClose: Subject<ChartData> = new Subject();
    private readonly data: ChartData = new ChartData();
    private name = "";
    private type: ChartType;
    private chartTypes: string[];
    private sensorCount = 0;
    private period: ChartPeriod;
    private chartPeriods: string[];
    private color: any;
    private colorSchemes: ColorScheme[];

    public constructor(private readonly bsModalRef: BsModalRef) {
    }

    public ngOnInit(): void {
        this.name = this.data.name;
        this.type = this.data.type;
        this.chartTypes = Object.keys(ChartType).map((e) => ChartType[e]);
        this.sensorCount = this.data.sensorCount;
        this.period = this.data.period;
        this.chartPeriods = Object.keys(ChartPeriod).map((e) => ChartPeriod[e]);
        this.color = this.data.color;
        this.colorSchemes = ColorSchemeService.getColorSchemes();
    }

    private onConfirm(): void {
        this.onClose.next(
            new ChartData(
                this.name,
                this.type,
                this.sensorCount,
                this.period,
                this.color
            )
        );
        this.bsModalRef.hide();
    }

    private onCancel(): void {
        this.onClose.next(null);
        this.bsModalRef.hide();
    }
}
