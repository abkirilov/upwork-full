import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap";
import { Subject } from "rxjs";
import { WidgetItem } from "../../../model/widget-item";
import { WidgetItemService } from "../../../service/widget-item.service";

@Component({
    templateUrl: "./add-widget.component.html"
})
export class AddWidgetComponent implements OnInit {
    private readonly onClose: Subject<WidgetItem> = new Subject();
    private widgets: WidgetItem[] = [];
    private widget: WidgetItem;

    public constructor(private readonly bsModalRef: BsModalRef) {
    }

    public ngOnInit(): void {
        this.widgets = WidgetItemService.getWidgets();
        this.widget = this.widgets[0];
    }

    private onConfirm(): void {
        this.onClose.next(this.widget);
        this.bsModalRef.hide();
    }

    private onCancel(): void {
        this.onClose.next(null);
        this.bsModalRef.hide();
    }
}
