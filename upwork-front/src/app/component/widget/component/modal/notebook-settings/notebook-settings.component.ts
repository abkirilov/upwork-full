import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap";
import { Subject } from "rxjs";
import { NotebookData } from "../../../model/notebook-data";

@Component({
    templateUrl: "./notebook-settings.component.html",
})
export class NotebookSettingsComponent implements OnInit {
    private readonly onClose: Subject<NotebookData> = new Subject();
    private readonly data: NotebookData = new NotebookData();
    private name = "";
    private note = "";

    public constructor(private readonly bsModalRef: BsModalRef) {
    }

    public ngOnInit(): void {
        this.name = this.data.name;
        this.note = this.data.note;
    }

    private onConfirm(): void {
        this.onClose.next(new NotebookData(this.name, this.note));
        this.bsModalRef.hide();
    }

    private onCancel(): void {
        this.onClose.next(null);
        this.bsModalRef.hide();
    }
}
