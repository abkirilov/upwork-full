import { Component, OnInit } from "@angular/core";
import { CompactType, DisplayGrid, GridsterConfig, GridType } from "angular-gridster2";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { environment } from "../../../../../environments/environment";
import { WidgetItem } from "../../model/widget-item";
import { AddWidgetComponent } from "../modal/add-widget/add-widget.component";

@Component({
    styleUrls: ["./dashboard.component.scss"],
    templateUrl: "./dashboard.component.html"
})
export class DashboardComponent implements OnInit {
    private bsModalRef: BsModalRef;
    private dashboard: WidgetItem[] = [];
    private gridsterConfig: GridsterConfig;

    public constructor(private readonly bsModalService: BsModalService) {
    }

    public ngOnInit(): void {
        this.gridsterConfig = {
            compactType: CompactType.CompactUpAndLeft,
            disableWarnings: environment.production,
            displayGrid: DisplayGrid.OnDragAndResize,
            draggable: {
                enabled: true
            },
            fixedRowHeight: 360,
            gridType: GridType.VerticalFixed,
            itemChangeCallback: this.resizeWidget(),
            itemResizeCallback: this.resizeWidget(),
            maxCols: 2,
            minCols: 2,
            mobileBreakpoint: 440,
            pushItems: true,
            resizable: {
                enabled: true
            },
            swap: true
        };
    }

    private resizeWidget(): any {
        return () => {
            const resizeEvent = window.document.createEvent("UIEvents");
            resizeEvent.initUIEvent("resize", true, false, window, 0);
            window.dispatchEvent(resizeEvent);
        };
    }

    private addWidget(): void {
        this.bsModalRef = this.bsModalService.show(AddWidgetComponent);
        this.bsModalRef.content.onClose.subscribe((widget) => {
            if (widget !== null && widget !== undefined) {
                this.dashboard.push(this.bsModalRef.content.widget);
            }
        });
    }

    private removeWidget(widget: WidgetItem): void {
        this.dashboard.splice(this.dashboard.indexOf(widget), 1);
    }

    private clearDashboard(): void {
        this.dashboard = [];
    }
}
