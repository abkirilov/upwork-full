import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { Subject } from "rxjs";
import { sensorBar, sensorLine } from "../../../../service/chart-data";
import { ChartData } from "../../model/chart-data";
import { ChartType } from "../../model/chart-type";
import { WidgetComponent } from "../../model/widget-component";
import { ChartSettingsComponent } from "../modal/chart-settings/chart-settings.component";

export enum Label {
    SENSOR = "Sensor",
    TEMPERATURE = "Temperature",
    TIME = "Time"
}

@Component({
    selector: "app-chart-widget-container",
    styleUrls: ["./chart-widget.component.scss"],
    templateUrl: "./chart-widget.component.html"
})
export class ChartWidgetComponent implements WidgetComponent, OnInit {
    public data: ChartData;
    private isLine = true;
    private results: any = [];
    private colorScheme: any;
    private bsModalRef: BsModalRef;

    private readonly gradient: boolean = true;
    private readonly legend: boolean = true;
    private readonly legendTitle: string = Label.SENSOR;
    private readonly showXAxisLabel: boolean = true;
    private readonly showYAxisLabel: boolean = true;
    private readonly timeline: boolean = true;
    private readonly xAxis: boolean = true;
    private readonly xAxisLabelBar: string = Label.TEMPERATURE;
    private readonly xAxisLabelLine: string = Label.TIME;
    private readonly yAxis: boolean = true;
    private readonly yAxisLabelBar: string = Label.SENSOR;
    private readonly yAxisLabelLine: string = Label.TEMPERATURE;

    public constructor(private readonly bsModalService: BsModalService) {
    }

    public openSettings(): Subject<ChartData> {
        this.bsModalRef = this.bsModalService.show(ChartSettingsComponent, {
            initialState: {
                data: this.data
            }
        });
        return this.bsModalRef.content.onClose;
    }

    public reloadData(): void {
        this.isLine = this.data.type === ChartType.LINE;
        this.results = this.isLine
            ? sensorLine.slice(0, this.data.sensorCount)
            : sensorBar.slice(0, this.data.sensorCount);
        this.colorScheme = {
            domain: this.data.color.colors
        };
    }

    public ngOnInit(): void {
        this.reloadData();
    }

    private onSelect(data): void {
        alert(`Item clicked: ${JSON.stringify(data)}`);
    }
}
