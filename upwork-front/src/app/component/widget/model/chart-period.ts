export enum ChartPeriod {
    DAY = "Day",
    WEEK = "Week",
    MONTH = "Month",
    YEAR = "Year"
}
