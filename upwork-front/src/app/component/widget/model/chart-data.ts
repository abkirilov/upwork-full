import { ChartPeriod } from "./chart-period";
import { ChartType } from "./chart-type";
import { ColorScheme } from "./color-scheme";

export class ChartData {
    public constructor(
        public name?: string,
        public type?: ChartType,
        public sensorCount?: number,
        public period?: ChartPeriod,
        public color?: ColorScheme
    ) {
    }
}
