import { Subject } from "rxjs";

export abstract class WidgetComponent {
    public data: any;

    public abstract openSettings(): Subject<any>;

    public abstract reloadData(): void;
}
