import { Type } from "@angular/core";
import { GridsterItem } from "angular-gridster2";

export class WidgetItem implements GridsterItem {
    public cols = 1;
    public rows = 1;
    public x = 0;
    public y = 0;

    public constructor(
        public component?: Type<any>,
        public data?: any
    ) {
    }
}
