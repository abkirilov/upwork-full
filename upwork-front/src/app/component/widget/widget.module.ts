import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { GridsterModule } from "angular-gridster2";
import { ModalModule } from "ngx-bootstrap";
import { NgxSpinnerModule } from "ngx-spinner";
import { AppRoutingModule } from "../../app-routing.module";
import { ChartWidgetComponent } from "./component/chart-widget/chart-widget.component";
import { DashboardComponent } from "./component/dashboard/dashboard.component";
import { AddWidgetComponent } from "./component/modal/add-widget/add-widget.component";
import { ChartSettingsComponent } from "./component/modal/chart-settings/chart-settings.component";
import { NotebookSettingsComponent } from "./component/modal/notebook-settings/notebook-settings.component";
import { NotebookWidgetComponent } from "./component/notebook-widget/notebook-widget.component";
import { WidgetContainerComponent } from "./component/widget-container/widget-container.component";
import { WidgetDirective } from "./directive/widget.directive";

@NgModule({
    declarations: [
        AddWidgetComponent,
        ChartSettingsComponent,
        ChartWidgetComponent,
        DashboardComponent,
        NotebookSettingsComponent,
        NotebookWidgetComponent,
        WidgetContainerComponent,
        WidgetDirective
    ],
    entryComponents: [
        AddWidgetComponent,
        ChartSettingsComponent,
        ChartWidgetComponent,
        NotebookSettingsComponent,
        NotebookWidgetComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        GridsterModule,
        HttpClientModule,
        ModalModule.forRoot(),
        NgSelectModule,
        NgxChartsModule,
        NgxSpinnerModule
    ]
})
export class WidgetModule {
}
