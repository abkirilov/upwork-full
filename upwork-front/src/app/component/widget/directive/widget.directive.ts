import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: "[widget-body]"
})
export class WidgetDirective {
    public constructor(public readonly viewContainerRef: ViewContainerRef) {
    }
}
