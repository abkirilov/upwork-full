import { Injectable } from "@angular/core";
import { ChartWidgetComponent } from "../component/chart-widget/chart-widget.component";
import { NotebookWidgetComponent } from "../component/notebook-widget/notebook-widget.component";
import { ChartData } from "../model/chart-data";
import { ChartPeriod } from "../model/chart-period";
import { ChartType } from "../model/chart-type";
import { NotebookData } from "../model/notebook-data";
import { WidgetItem } from "../model/widget-item";
import { ColorSchemeService } from "./color-scheme.service";

@Injectable({
    providedIn: "root"
})
export class WidgetItemService {
    public static getWidgets(): WidgetItem[] {
        return [
            new WidgetItem(
                ChartWidgetComponent,
                new ChartData(
                    "Bar Chart",
                    ChartType.BAR,
                    4,
                    ChartPeriod.DAY,
                    ColorSchemeService.getColorSchemes()[0]
                )
            ),
            new WidgetItem(
                ChartWidgetComponent,
                new ChartData(
                    "Line Chart",
                    ChartType.LINE,
                    3,
                    ChartPeriod.DAY,
                    ColorSchemeService.getColorSchemes()[0]
                )
            ),
            new WidgetItem(
                NotebookWidgetComponent,
                new NotebookData("Notebook", "new note...")
            )
        ];
    }
}
