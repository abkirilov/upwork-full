import { Injectable } from "@angular/core";
import { ColorScheme } from "../model/color-scheme";

@Injectable({
    providedIn: "root"
})
export class ColorSchemeService {
    public static getColorSchemes(): ColorScheme[] {
        return [
            new ColorScheme("Bootstrap", [
                "#007bff",
                "#17a2b8",
                "#28a745",
                "#ffc107",
                "#dc3545",
                "#343a40",
                "#6c757d",
                "#f8f9fa",
                "#ffffff"
            ]),
            new ColorScheme("Rainbow", [
                "#ff0000",
                "#ffa500",
                "#ffff00",
                "#008000",
                "#0000ff",
                "#4b0082",
                "#ee82ee"
            ])
        ];
    }
}
