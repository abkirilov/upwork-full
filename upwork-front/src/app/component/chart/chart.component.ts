import { Component } from "@angular/core";
import { multi, single } from "../../service/chart-data";

export enum Label {
    COUNTRY = "Country",
    POPULATION = "Population",
    YEAR = "Year"
}

@Component({
    templateUrl: "./chart.component.html"
})
export class ChartComponent {
    private readonly gradient: boolean = true;
    private readonly legend: boolean = true;
    private readonly legendTitle: string = Label.COUNTRY;
    private readonly showXAxisLabel: boolean = true;
    private readonly showYAxisLabel: boolean = true;
    private readonly timeline: boolean = true;
    private readonly xAxis: boolean = true;
    private readonly xAxisLabelBar: string = Label.POPULATION;
    private readonly xAxisLabelLine: string = Label.YEAR;
    private readonly yAxis: boolean = true;
    private readonly yAxisLabelBar: string = Label.COUNTRY;
    private readonly yAxisLabelLine: string = Label.POPULATION;

    private readonly multi: any[];
    private readonly single: any[];
    private readonly viewBar: any[] = [900, 200];
    private readonly viewLine: any[] = [900, 300];

    private readonly colorScheme = {
        domain: [
            "#007bff",
            "#17a2b8",
            "#28a745",
            "#ffc107",
            "#dc3545",
            "#343a40",
            "#6c757d"
        ]
    };

    public constructor() {
        Object.assign(this, {multi, single});
    }

    private onSelect(data): void {
        alert(`Item clicked: ${JSON.stringify(data)}`);
    }
}
