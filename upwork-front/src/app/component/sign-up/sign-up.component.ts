import { Component } from "@angular/core";
import { Message } from "../../model/message";
import { User } from "../../model/user";
import { AuthService } from "../../service/auth.service";
import { Utils } from "../../service/utils";

@Component({
    styleUrls: ["./sign-up.component.scss"],
    templateUrl: "./sign-up.component.html"
})
export class SignUpComponent {
    private readonly message: Message = new Message();
    private showError = false;
    private showForm = true;
    private readonly user: User = new User();

    public constructor(private readonly authService: AuthService) {
    }

    private signUpUser(): void {
        this.authService.signUpUser(this.user).subscribe(
            (data) => {
                this.showError = false;
                this.showForm = false;
            },
            (err) => {
                this.message.text = Utils.getErrorMessage(err);
                this.showError = true;
                this.showForm = true;
            }
        );
    }
}
