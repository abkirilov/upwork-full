import { Component, OnInit } from "@angular/core";
import { AuthTestService } from "../../service/auth-test.service";
import { Utils } from "../../service/utils";

@Component({
    templateUrl: "./board-moderator.component.html"
})
export class BoardModeratorComponent implements OnInit {
    private content = "";

    public constructor(private readonly authTestService: AuthTestService) {
    }

    public ngOnInit(): void {
        this.authTestService.getModeratorBoard().subscribe(
            (data) => {
                this.content = data;
            },
            (err) => {
                this.content = Utils.getErrorText(err);
            }
        );
    }
}
