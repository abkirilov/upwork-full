import { Component, OnInit } from "@angular/core";
import { Message } from "../../model/message";
import { User } from "../../model/user";
import { AuthService } from "../../service/auth.service";
import { DataSharingService } from "../../service/data-sharing.service";
import { TokenStorageService } from "../../service/token-storage.service";
import { Utils } from "../../service/utils";

@Component({
    styleUrls: ["./log-in.component.scss"],
    templateUrl: "./log-in.component.html"
})
export class LogInComponent implements OnInit {
    private readonly message: Message = new Message();
    private roles = "";
    private showError = false;
    private showForm = true;
    private readonly user: User = new User();
    private username = "";

    public constructor(
        private readonly authService: AuthService,
        private readonly dataSharingService: DataSharingService,
        private readonly tokenStorageService: TokenStorageService
    ) {
    }

    public ngOnInit(): void {
        this.loadJwtToken();
    }

    private loadJwtToken(): void {
        const jwtToken = this.tokenStorageService.getJwtToken();
        if (jwtToken !== null) {
            this.showForm = false;
            this.roles = jwtToken.roles.join(", ");
            this.username = jwtToken.username;
        }
    }

    private logInUser(): void {
        this.authService.logInUser(this.user).subscribe(
            (data) => {
                this.tokenStorageService.saveJwtToken(data);
                this.dataSharingService.isLogIn.next(true);
                this.loadJwtToken();
            },
            (err) => {
                this.message.text = Utils.getErrorMessage(err);
                this.showError = true;
                this.showForm = true;
            }
        );
    }
}
