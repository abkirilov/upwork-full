import { Component, OnInit } from "@angular/core";
import { AuthTestService } from "../../service/auth-test.service";
import { Utils } from "../../service/utils";

@Component({
    templateUrl: "./board-user.component.html"
})
export class BoardUserComponent implements OnInit {
    private content = "";

    public constructor(private readonly authTestService: AuthTestService) {
    }

    public ngOnInit(): void {
        this.authTestService.getUserContent().subscribe(
            (data) => {
                this.content = data;
            },
            (err) => {
                this.content = Utils.getErrorText(err);
            }
        );
    }
}
