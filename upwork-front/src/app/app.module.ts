import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { ModalModule } from "ngx-bootstrap";
import { NgxSpinnerModule } from "ngx-spinner";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BoardAdminComponent } from "./component/board-admin/board-admin.component";
import { BoardModeratorComponent } from "./component/board-moderator/board-moderator.component";
import { BoardUserComponent } from "./component/board-user/board-user.component";
import { ChartComponent } from "./component/chart/chart.component";
import { LogInComponent } from "./component/log-in/log-in.component";
import { ErrorNotificationComponent } from "./component/modal/error-notification/error-notification.component";
import { NavbarComponent } from "./component/navbar/navbar.component";
import { ProfileComponent } from "./component/profile/profile.component";
import { SignUpComponent } from "./component/sign-up/sign-up.component";
import { WidgetModule } from "./component/widget/widget.module";
import { AuthInterceptor } from "./interceptor/auth.interceptor";
import { ErrorInterceptor } from "./interceptor/error.interceptor";
import { SpinnerInterceptor } from "./interceptor/spinner.interceptor";

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        BoardAdminComponent,
        BoardModeratorComponent,
        BoardUserComponent,
        ChartComponent,
        ErrorNotificationComponent,
        LogInComponent,
        NavbarComponent,
        ProfileComponent,
        SignUpComponent
    ],
    entryComponents: [
        ErrorNotificationComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ModalModule.forRoot(),
        NgxChartsModule,
        NgxSpinnerModule,
        WidgetModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi: true}
    ]
})
export class AppModule {
}
