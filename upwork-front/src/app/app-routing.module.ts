import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BoardAdminComponent } from "./component/board-admin/board-admin.component";
import { BoardModeratorComponent } from "./component/board-moderator/board-moderator.component";
import { BoardUserComponent } from "./component/board-user/board-user.component";
import { ChartComponent } from "./component/chart/chart.component";
import { LogInComponent } from "./component/log-in/log-in.component";
import { ProfileComponent } from "./component/profile/profile.component";
import { SignUpComponent } from "./component/sign-up/sign-up.component";
import { DashboardComponent } from "./component/widget/component/dashboard/dashboard.component";

export enum Url {
    ADMIN = "admin",
    CHART = "chart",
    HOME = "home",
    LOGIN = "login",
    MODERATOR = "moderator",
    PROFILE = "profile",
    REGISTER = "register",
    USER = "user"
}

const routes: Routes = [
    {path: Url.ADMIN, component: BoardAdminComponent},
    {path: Url.CHART, component: ChartComponent},
    {path: Url.HOME, component: DashboardComponent},
    {path: Url.LOGIN, component: LogInComponent},
    {path: Url.MODERATOR, component: BoardModeratorComponent},
    {path: Url.PROFILE, component: ProfileComponent},
    {path: Url.REGISTER, component: SignUpComponent},
    {path: Url.USER, component: BoardUserComponent},
    {path: "**", redirectTo: Url.HOME}
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {
}
