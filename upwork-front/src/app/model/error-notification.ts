export class ErrorNotification {
    public constructor(
        public code?: string,
        public text?: string,
        public url?: string
    ) {
    }
}
