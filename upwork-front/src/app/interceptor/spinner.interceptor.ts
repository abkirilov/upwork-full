import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
    public constructor(private readonly ngxSpinnerService: NgxSpinnerService) {
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.ngxSpinnerService.show().then((r) => null);
        return next
            .handle(req)
            .timeout(environment.timeout_request)
            .pipe(tap(
                (event) => {
                    if (event instanceof HttpResponse) {
                        this.ngxSpinnerService.hide().then((r) => null);
                    }
                },
                (err) => {
                    this.ngxSpinnerService.hide().then((r) => null);
                })
            );
    }
}
