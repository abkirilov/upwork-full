import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenStorageService } from "../service/token-storage.service";

const BEARER = "Bearer ";
const TOKEN_HEADER_KEY = "Authorization";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    public constructor(
        private readonly tokenStorageService: TokenStorageService
    ) {
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const jwtToken = this.tokenStorageService.getToken();
        let httpRequest = req;
        if (jwtToken !== null) {
            httpRequest = req.clone({
                headers: req.headers.set(TOKEN_HEADER_KEY, `${BEARER}${jwtToken}`)
            });
        }
        return next.handle(httpRequest);
    }
}
