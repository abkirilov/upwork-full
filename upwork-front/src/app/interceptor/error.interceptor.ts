import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/timeout";
import { Observable } from "rxjs/Observable";
import { TimeoutError } from "rxjs/util/TimeoutError";
import { environment } from "../../environments/environment";
import { ErrorNotificationComponent } from "../component/modal/error-notification/error-notification.component";
import { ErrorNotification } from "../model/error-notification";
import { Utils } from "../service/utils";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    public constructor(private readonly bsModalService: BsModalService) {
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
            .handle(req)
            .timeout(environment.timeout_request)
            .catch((httpError) => {
                if (httpError instanceof TimeoutError) {
                    if (this.bsModalService.getModalsCount() < 1) {
                        this.bsModalService.show(ErrorNotificationComponent, {
                            initialState: {
                                errorNotification: new ErrorNotification(httpError.name, httpError.message, req.url)
                            },
                            backdrop: false
                        });
                    }
                    return Observable.throwError(httpError);
                }
                if (httpError instanceof HttpErrorResponse) {
                    const httpStatus = httpError.status.toString();
                    if (!httpStatus.startsWith("4") && this.bsModalService.getModalsCount() < 1) {
                        this.bsModalService.show(ErrorNotificationComponent, {
                            initialState: {
                                errorNotification: new ErrorNotification(
                                    httpStatus,
                                    Utils.getErrorText(httpError),
                                    httpError.url
                                )
                            },
                            backdrop: false
                        });
                    }
                }
                return Observable.throwError(httpError);
            });
    }
}
