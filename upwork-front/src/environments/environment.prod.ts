export const environment = {
    production: true,
    rest_path: "http://localhost:8080/api",
    timeout_notification: 10000,
    timeout_request: 1000
};
