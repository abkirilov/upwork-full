export const environment = {
    production: false,
    rest_path: "http://localhost:8080/api",
    timeout_notification: 100000,
    timeout_request: 1000
};
