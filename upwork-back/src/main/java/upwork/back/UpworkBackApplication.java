package upwork.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpworkBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpworkBackApplication.class, args);
	}
}
