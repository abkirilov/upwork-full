package upwork.back.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import upwork.back.payload.request.LogInRequest;
import upwork.back.payload.request.SignUpRequest;
import upwork.back.payload.response.JwtResponse;
import upwork.back.payload.response.MessageResponse;
import upwork.back.service.AuthService;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
@RequestMapping("/auth/sign")
public class AuthController {

	private final AuthService authService;

	@PostMapping("/in")
	public ResponseEntity<JwtResponse> logInUser(@Valid
												 @RequestBody LogInRequest logInRequest) {
		return ResponseEntity.ok(authService.getJwtResponse(logInRequest));
	}

	@PostMapping("/up")
	public ResponseEntity<MessageResponse> signUpUser(@Valid
													  @RequestBody SignUpRequest signUpRequest) {
		return authService.registerUser(signUpRequest);
	}
}
