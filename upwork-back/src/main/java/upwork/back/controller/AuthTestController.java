package upwork.back.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("SameReturnValue")
@RestController
@RequestMapping("/auth/test")
public class AuthTestController {

	@GetMapping("/all")
	public String testAllAccess() {
		return "Public Content";
	}

	@GetMapping("/user")
	@PreAuthorize("hasAnyRole('USER', 'MODERATOR', 'ADMIN')")
	public String testUserAccess() {
		return "User Content";
	}

	@GetMapping("/moderator")
	@PreAuthorize("hasAnyRole('MODERATOR', 'ADMIN')")
	public String testModeratorAccess() {
		return "Moderator Board";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public String testAdminAccess() {
		return "Admin Board";
	}
}
