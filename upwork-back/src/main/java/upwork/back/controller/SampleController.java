package upwork.back.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import upwork.back.payload.request.SampleRequest;
import upwork.back.payload.response.SampleResponse;
import upwork.back.service.SampleService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/samples")
public class SampleController {

	private final SampleService sampleService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SampleResponse>> getSampleList(@RequestParam(required = false) String title) {
		List<SampleResponse> sampleList = sampleService.getSampleList(title);
		return sampleList.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(sampleList, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<SampleResponse> getXmlSample(@PathVariable("id") long id) {
		SampleResponse sampleResponse = sampleService.getSample(id);
		return sampleResponse == null ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(sampleResponse, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<SampleResponse> createSample(@RequestBody SampleRequest sampleRequest) {
		return new ResponseEntity<>(sampleService.saveSample(sampleRequest), HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<SampleResponse> updateSample(@RequestBody SampleRequest sampleRequest) {
		return new ResponseEntity<>(sampleService.saveSample(sampleRequest), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
		sampleService.deleteSample(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping
	public ResponseEntity<HttpStatus> deleteAllTutorials() {
		sampleService.deleteAll();
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/active")
	public ResponseEntity<List<SampleResponse>> getSampleListIsActive() {
		List<SampleResponse> sampleList = sampleService.getSampleListIsActive();
		return sampleList.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(sampleList, HttpStatus.OK);
	}
}
