package upwork.back.payload.response;

import lombok.Data;

@Data
public class SampleResponse {

	private Long id;
	private String title;
	private String description;
	private boolean isActive;
}
