package upwork.back.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SampleRequest {

	private Long id;
	private String title;
	private String description;

	@JsonProperty
	private boolean isActive;
}
