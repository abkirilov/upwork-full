package upwork.back.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class LogInRequest {

	@NotBlank
	@Size(min = 3, max = 100)
	private String username;

	@NotBlank
	@Size(min = 3, max = 100)
	private String password;
}
