package upwork.back.payload.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper = true)
public class SignUpRequest extends LogInRequest {

	@Email
	@NotBlank
	@Size(max = 100)
	private String email;
}
