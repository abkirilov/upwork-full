package upwork.back.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "USERS")
public class User {

	@Id
	@GenericGenerator(name = "USERS_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "USERS_ID_SEQ"),
			@Parameter(name = "increment_size", value = "1"),
			@Parameter(name = "initial_value", value = "1"),
	})
	@GeneratedValue(generator = "USERS_ID_SEQ")
	private Long id;

	@NotBlank
	@Size(min = 3, max = 100)
	@Column(name = "USERNAME", unique = true)
	private String username;

	@Email
	@NotBlank
	@Size(max = 100)
	@Column(name = "EMAIL", unique = true)
	private String email;

	@NotBlank
	@Size(min = 3, max = 100)
	@Column(name = "PASSWORD")
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "USER_ROLES",
			joinColumns = @JoinColumn(name = "USER_ID"),
			inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
	private Set<Role> roleSet = new HashSet<>();
}
