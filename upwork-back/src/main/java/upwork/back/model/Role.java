package upwork.back.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "ROLES")
public class Role {

	@Id
	@GenericGenerator(name = "ROLES_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "ROLES_ID_SEQ"),
			@Parameter(name = "increment_size", value = "1"),
			@Parameter(name = "initial_value", value = "1"),
	})
	@GeneratedValue(generator = "ROLES_ID_SEQ")
	@Column(name = "ID")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "NAME")
	private RoleType name;
}
