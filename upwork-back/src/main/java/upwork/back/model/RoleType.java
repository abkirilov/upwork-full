package upwork.back.model;

@SuppressWarnings("unused")
public enum RoleType {
	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
}
