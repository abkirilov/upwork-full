package upwork.back.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Slf4j
@Component
public class JwtUtils {

	private static final String BEARER = "Bearer ";

	@Value("${jwt.secret}")
	private String jwtSecret;

	@Value("${jwt.expiry}")
	private int jwtExpiry;

	String getJwtToken(HttpServletRequest request) {
		String headerAuth = request.getHeader("Authorization");
		return StringUtils.hasText(headerAuth) && headerAuth.startsWith(BEARER) ? headerAuth.substring(BEARER.length()) : null;
	}

	boolean validateJwtToken(String token) {
		try {
			getClaimsJws(token);
			return true;
		} catch (SignatureException e) {
			log.error("Error: Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			log.error("Error: Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			log.error("Error: JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			log.error("Error: JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			log.error("Error: JWT claims string is empty: {}", e.getMessage());
		}
		return false;
	}

	String getUserNameFromJwtToken(String token) {
		return getClaimsJws(token).getBody().getSubject();
	}

	private Jws<Claims> getClaimsJws(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
	}

	public String generateJwtToken(Authentication authentication) {
		return Jwts.builder()
				.setSubject(((UserDetails) authentication.getPrincipal()).getUsername())
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpiry))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}
}
