package upwork.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upwork.back.model.Sample;

import java.util.List;

@Repository
public interface SampleRepository extends JpaRepository<Sample, Long> {

	List<Sample> findByTitleContaining(String title);

	@SuppressWarnings("SpringDataMethodInconsistencyInspection")
	List<Sample> findByIsActive(boolean isActive);
}
