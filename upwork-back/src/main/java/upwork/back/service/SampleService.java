package upwork.back.service;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;
import upwork.back.model.Sample;
import upwork.back.payload.request.SampleRequest;
import upwork.back.payload.response.SampleResponse;
import upwork.back.repository.SampleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class SampleService {

	private final MapperFacade mapperFacade;
	private final SampleRepository sampleRepository;

	public List<SampleResponse> getSampleList(String title) {
		List<Sample> sampleList = new ArrayList<>(title == null ? sampleRepository.findAll() : sampleRepository.findByTitleContaining(title));
		return mapperFacade.mapAsList(sampleList, SampleResponse.class);
	}

	public SampleResponse getSample(Long id) {
		Optional<Sample> sample = sampleRepository.findById(id);
		return sample.map(s -> mapperFacade.map(s, SampleResponse.class)).orElse(null);
	}

	public SampleResponse saveSample(SampleRequest sampleRequest) {
		Sample sample = mapperFacade.map(sampleRequest, Sample.class);
		sample = sampleRepository.save(sample);
		return mapperFacade.map(sample, SampleResponse.class);
	}

	public void deleteSample(long id) {
		sampleRepository.deleteById(id);
	}

	public void deleteAll() {
		sampleRepository.deleteAll();
	}

	public List<SampleResponse> getSampleListIsActive() {
		List<Sample> sampleList = sampleRepository.findByIsActive(true);
		return mapperFacade.mapAsList(sampleList, SampleResponse.class);
	}
}
