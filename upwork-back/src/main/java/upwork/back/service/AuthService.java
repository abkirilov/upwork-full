package upwork.back.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import upwork.back.model.Role;
import upwork.back.model.RoleType;
import upwork.back.model.User;
import upwork.back.payload.request.LogInRequest;
import upwork.back.payload.request.SignUpRequest;
import upwork.back.payload.response.JwtResponse;
import upwork.back.payload.response.MessageResponse;
import upwork.back.repository.RoleRepository;
import upwork.back.repository.UserRepository;
import upwork.back.security.jwt.JwtUtils;
import upwork.back.security.model.UserDetailsImpl;

import java.util.Collections;
import java.util.Set;

@AllArgsConstructor
@Service
public class AuthService {

	private final AuthenticationManager authenticationManager;
	private final JwtUtils jwtUtils;
	private final PasswordEncoder passwordEncoder;
	private final RoleRepository roleRepository;
	private final UserRepository userRepository;

	public JwtResponse getJwtResponse(LogInRequest logInRequest) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(logInRequest.getUsername(), logInRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwtToken = jwtUtils.generateJwtToken(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		return new JwtResponse(jwtToken, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), userDetails.getRoleList());
	}

	public ResponseEntity<MessageResponse> registerUser(SignUpRequest signUpRequest) {
		String username = signUpRequest.getUsername();
		if (userRepository.existsByUsername(username)) {
			return ResponseEntity.badRequest().body(new MessageResponse(String.format("Username: \"%s\" is already taken", username)));
		}
		String email = signUpRequest.getEmail();
		if (userRepository.existsByEmail(email)) {
			return ResponseEntity.badRequest().body(new MessageResponse(String.format("Email: \"%s\" is already in use", email)));
		}
		String password = passwordEncoder.encode(signUpRequest.getPassword());
		Set<Role> roleSet = Collections.singleton(roleRepository.findByName(RoleType.ROLE_USER));
		userRepository.save(new User(null, username, email, password, roleSet));
		return ResponseEntity.ok(new MessageResponse("User registered successfully"));
	}
}
