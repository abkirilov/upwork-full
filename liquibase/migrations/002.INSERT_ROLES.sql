--liquibase formatted sql
--changeset akirilov:002.INSERT_ROLES

insert into roles(id, name)
values (nextval('roles_id_seq'), 'ROLE_USER');
insert into roles(id, name)
VALUES (nextval('roles_id_seq'), 'ROLE_MODERATOR');
insert into roles(id, name)
VALUES (nextval('roles_id_seq'), 'ROLE_ADMIN');

--rollback delete from roles;
