--liquibase formatted sql
--changeset akirilov:001.CREATE_DDL

create table samples
(
    id          int8 not null,
    description varchar(255),
    is_active   boolean,
    title       varchar(255),
    primary key (id)
);
create sequence samples_id_seq start 1 increment 1;

create table roles
(
    id   int8 not null,
    name varchar(255),
    primary key (id)
);
create sequence roles_id_seq start 1 increment 1;

create table users
(
    id       int8 not null,
    email    varchar(100),
    password varchar(100),
    username varchar(100),
    primary key (id)
);
create sequence users_id_seq start 1 increment 1;
alter table users
    add constraint users_email_uq unique (email);
alter table users
    add constraint users_username_uq unique (username);

create table user_roles
(
    user_id int8 not null,
    role_id int8 not null,
    primary key (user_id, role_id)
);
alter table user_roles
    add constraint user_roles_role_id_fk foreign key (role_id) references roles;
alter table user_roles
    add constraint user_roles_user_id_fk foreign key (user_id) references users;

--rollback drop table if exists user_roles cascade;
--rollback drop table if exists users cascade;
--rollback drop sequence if exists users_id_seq;
--rollback drop table if exists roles cascade;
--rollback drop sequence if exists roles_id_seq;
--rollback drop table if exists samples cascade;
--rollback drop sequence if exists samples_id_seq;
