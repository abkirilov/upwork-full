# Upwork full-stack project
## 1. docker-postgres
    1. postgres_pull.cmd
    2. postgres_run.cmd
    3. postgres_start.cmd
## 2. docker-sonar
    1. sonar_pull.cmd
    2. sonar_run.cmd
    3. sonar_start.cmd
    4. gradlew sonarqube
## 2. liquibase
    gradlew update
    or update.cmd
## 3. upwork-back
    gradlew bootRun
## 4. upwork-front
    ng server
    or npm run start
## 5. profit!
[Angular](http://localhost:4200/home)

[Swagger](http://localhost:8080/api/swagger-ui.html)

[SonarQube](http://localhost:9000/dashboard?id=upwork-full)
